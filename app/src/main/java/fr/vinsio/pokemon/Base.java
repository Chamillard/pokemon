package fr.vinsio.pokemon;

public class Base {

    private int HP;
    private int Attack;
    private int Defense;
    private int SpAttack;
    private int SpDefense;
    private int Speed;

    public Base() {}

    public Base(int HP, int Attack, int Defense, int SpAttack, int SpDefense, int Speed) {
        this.HP = HP;
        this.Attack = Attack;
        this.Defense = Defense;
        this.SpAttack = SpAttack;
        this.SpDefense = SpDefense;
        this.Speed = Speed;
    }

    public int getHP() {
        return HP;
    }

    public void setHP(int HP) {
        this.HP = HP;
    }

    public int getAttack() {
        return Attack;
    }

    public void setAttack(int attack) {
        Attack = attack;
    }

    public int getDefense() {
        return Defense;
    }

    public void setDefense(int defense) {
        Defense = defense;
    }

    public int getSpAttack() {
        return SpAttack;
    }

    public void setSpAttack(int spAttack) {
        SpAttack = spAttack;
    }

    public int getSpDefense() {
        return SpDefense;
    }

    public void setSpDefense(int spDefense) {
        SpDefense = spDefense;
    }

    public int getSpeed() {
        return Speed;
    }

    public void setSpeed(int speed) {
        Speed = speed;
    }

    @Override
    public String toString() {
        return HP + "," + Attack  + "," + Defense  + "," + SpAttack + "," + SpDefense + "," + Speed ;
    }
}
